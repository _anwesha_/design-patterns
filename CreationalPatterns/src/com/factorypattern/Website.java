package com.factorypattern;

import java.util.ArrayList;
import java.util.List;

public abstract class Website {
	
	protected List<Page> pages=new ArrayList<>();
	
	public Website() {
		this.createWebsite();
	}

	public List<Page> getPages() {
		return pages;
	}
	
	public abstract void createWebsite();//this is the crux of the factory method, all the base classes will overwrite this method to generate objects.

}
