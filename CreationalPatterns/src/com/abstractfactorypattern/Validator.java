package com.abstractfactorypattern;

public interface Validator {
	
	public Boolean isValid(CreditCard creditCard);

}
