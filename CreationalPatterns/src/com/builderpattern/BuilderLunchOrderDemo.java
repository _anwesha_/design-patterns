package com.builderpattern;

public class BuilderLunchOrderDemo {

	public static void main(String[] args) {
		
		LunchOrder.Builder builder=new LunchOrder.Builder();
		//we can have any combination without having to write specific constructors for each, which can become a significant problem if we need to add new items.
		builder.bread("wheat").condiments("lettuce").dressing("mayo").meat("turkey");
//		builder.bread("wheat").condiments("lettuce");
//		builder.dressing("mayo").meat("turkey");
		
		LunchOrder lunchOrder=builder.build();
		
		System.out.println(lunchOrder.getBread());
		System.out.println(lunchOrder.getCondiments());
		System.out.println(lunchOrder.getDressing());
		System.out.println(lunchOrder.getMeat());

	}

}
