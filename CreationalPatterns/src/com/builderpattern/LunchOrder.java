package com.builderpattern;

public class LunchOrder {

	public static class Builder {
		private String bread;
		private String condiments;
		private String dressing;
		private String meat;

		public Builder() {//we can make this parameterised if we want to make atleast one or two component compulsory.

		}

		public LunchOrder build() {
			return new LunchOrder(this);
		}

		public Builder bread(String bread) {
			this.bread = bread;
			return this;// this is the Builder instance.
		}

		public Builder condiments(String condiments) {
			this.condiments = condiments;
			return this;// this is the Builder instance.
		}

		public Builder dressing(String dressing) {
			this.dressing = dressing;
			return this;// this is the Builder instance.
		}

		public Builder meat(String meat) {
			this.meat = meat;
			return this;// this is the Builder instance.
		}
	}

	private final String bread;
	private final String condiments;
	private final String dressing;
	private final String meat;

	private LunchOrder(Builder builder) {
		this.bread=builder.bread;
		this.condiments=builder.condiments;
		this.dressing=builder.dressing;
		this.meat=builder.meat;
	}

	public String getBread() {
		return bread;
	}

	public String getCondiments() {
		return condiments;
	}

	public String getDressing() {
		return dressing;
	}

	public String getMeat() {
		return meat;
	}
	
}
