package com.prototypepattern;

import java.util.HashMap;
import java.util.Map;

public class Registry {
	
	private Map<String,Item> items=new HashMap<String,Item>();
	
	public Registry() {
		loadItems();
	}
	
	public Item createItem(String type) {
		Item item=null;
		try {
			item=(Item)(items.get(type)).clone();
		}
		catch(CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return item;
	}
	
	private void loadItems() {//set default values here
		Movie movie=new Movie();//new keyword is used only here, and every time we need an instance of movie we clone this instance, hence a its a lot lighter weight object instantiation and a lot faster.
		movie.setTitle("Basic movie");
		movie.setPrice(24.34);
		movie.setRuntime("2hrs");
		items.put("Movie", movie);
		
		Book book=new Book();
		book.setNumberOfPages(390);
		book.setPrice(499.99);
		book.setTitle("Basic book");
		items.put("Book", book);
	}

}
