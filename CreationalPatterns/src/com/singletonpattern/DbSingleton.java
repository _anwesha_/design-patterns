package com.singletonpattern;

public class DbSingleton {

//	private static DbSingleton instance=new DbSingleton();
//	
//	private DbSingleton() {}
//	
//	public static DbSingleton getInstance() {
//		return instance;
//	}

	//lazily loaded(instance of Dbsingleton doesn't get created as soon as class is loaded, instead it's created only when required).
//	private static DbSingleton instance = null;
//
//	private DbSingleton() {
//	}
//
//	public static DbSingleton getInstance() {
//		if(instance==null) {
//			instance=new DbSingleton();
//		}
//		return instance;
//	}
	
	//making it lazily load and threadsafe
	private static volatile DbSingleton instance = null;

	private DbSingleton() {	//make sure reflection is not used.
		if(instance!=null) {
			throw new RuntimeException("Use getInstance() method to create.");
		}
	}

	public static DbSingleton getInstance() {
		if(instance==null) {
			synchronized (DbSingleton.class) {//checks if another class has a lock on the instance.
				if(instance==null) {
					instance=new DbSingleton();
				}
			}
		}
		return instance;
	}

}
