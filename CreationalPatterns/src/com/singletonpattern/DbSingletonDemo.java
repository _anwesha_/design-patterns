package com.singletonpattern;

public class DbSingletonDemo {

	public static void main(String[] args) {
		DbSingleton instance=DbSingleton.getInstance();
		System.out.println(instance);
		
		DbSingleton anotherInstance=DbSingleton.getInstance();//returns the same object as it is a singleton.
		System.out.println(anotherInstance);

	}

}
