package com.adapterpattern;

import java.util.ArrayList;
import java.util.List;

public class EmployeeClient {
	
	public List<Employee> getEmployeeList(){
		List<Employee> employees=new ArrayList<>();
		Employee employeeFromDB=new EmployeeDB("1234","John","Wick","jokn@wick.com");
		employees.add(employeeFromDB);
//		System.out.println(employees);
		EmployeeLdap employeeFromLdap=new EmployeeLdap("hs01", "Solo", "Han", "han@solo");
		employees.add(new EmployeeAdapterLdap(employeeFromLdap));
		EmployeeCSV employeeFromCSV=new EmployeeCSV("221,Sherlock,Holmes,sherlock@holmes");
		employees.add(new EmployeeAdapterCSV(employeeFromCSV));
		return employees;
	}

}
