package com.proxypattern;

public class TwitterDemo {

	public static void main(String[] args) {
		
		TwitterService service=(TwitterService)SecurityProxy.newInstance(new TwitterServiceStub());
		
		System.out.println(service.getTimeLine("bh5k"));

	}

}
