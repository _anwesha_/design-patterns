package com.flyweightpattern;

public class FlyweightInventoryDemo {

	public static void main(String[] args) {
		
		InventorySystem ims=new InventorySystem();
		
		ims.takeOrder("Roomba", 221);
		ims.takeOrder("Bose", 361);
		ims.takeOrder("Roomba", 231);
		ims.takeOrder("Bose", 365);
		ims.takeOrder("Samsung", 432);
		ims.takeOrder("Samsung", 412);
		ims.takeOrder("Roomba", 241);
		ims.takeOrder("Roomba", 211);
		ims.takeOrder("Bose", 360);
		ims.takeOrder("Samsung", 332);
		ims.takeOrder("Bose", 363);
		ims.takeOrder("Samsung", 422);
		
		ims.process();
		
		System.out.println(ims.report());

	}

}
