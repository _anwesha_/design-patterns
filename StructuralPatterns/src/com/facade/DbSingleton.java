package com.facade;

import java.sql.Connection;

public class DbSingleton {

	private static volatile DbSingleton instance = null;

	private DbSingleton() { // make sure reflection is not used.
		if (instance != null) {
			throw new RuntimeException("Use getInstance() method to create.");
		}
	}

	public static DbSingleton getInstance() {
		if (instance == null) {
			synchronized (DbSingleton.class) {// checks if another class has a lock on the instance.
				if (instance == null) {
					instance = new DbSingleton();
				}
			}
		}
		return instance;
	}

	public Connection getConnection() {
		System.out.println("Connection established.");
		return null;
	}

}
