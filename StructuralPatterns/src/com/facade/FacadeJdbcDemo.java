package com.facade;

import java.util.ArrayList;
import java.util.List;

import com.facade.JdbcFacade.Address;

public class FacadeJdbcDemo {

	public static void main(String[] args) {
		//facade cleans up our client code.
		JdbcFacade jdbcFacade=new JdbcFacade();
		jdbcFacade.createTable();
		System.out.println("Table created");
		jdbcFacade.insertIntoTable();
		System.out.println("Inserted into table");
		List<Address> addresses=new ArrayList<>();
		for(Address address:addresses) {
			System.out.println(address.getId()+" "+address.getStreetName()+" "+address.getCity());
		}

	}

}
