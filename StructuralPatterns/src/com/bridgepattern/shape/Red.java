package com.bridgepattern.shape;

public class Red implements Colour {

	@Override
	public void applyColour() {
		
		System.out.println("Applying red colour");

	}

}
