package com.bridgepattern.shape;

public interface Colour {
	
	public void applyColour();

}
