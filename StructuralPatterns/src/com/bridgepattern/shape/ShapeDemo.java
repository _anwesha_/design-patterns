package com.bridgepattern.shape;

public class ShapeDemo {

	public static void main(String[] args) {
		//this pattern helps implement diff attributes independent of each other so that each can have an independent growth. It bridesthe gap and removes the orthogonal problem of changing/adding a new feature in an attributes needs changes be made in the others as well.
		Colour blue=new Blue();
		Shape square=new Square(blue);
		
		Colour red=new Red();
		Shape circle=new Circle(red);
		
		square.applyColour();
		circle.applyColour();

	}

}
