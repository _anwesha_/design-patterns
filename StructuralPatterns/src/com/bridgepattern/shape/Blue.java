package com.bridgepattern.shape;

public class Blue implements Colour {

	@Override
	public void applyColour() {
		
		System.out.println("Applying blue colour");

	}

}
