package com.decoratorpattern;

public interface Sandwich {
	
	public String make();

}
