package com.observerpattern;

public class ObserverDemo {

	public static void main(String[] args) {
		
		Subject subject=new MessageStream();
		
		PhoneClient phoneClient=new PhoneClient(subject);
		TabletClient tabletClient=new TabletClient(subject);
		
		//if we call addMessage() method before making the tablet client, the phoneClient does not notify the tabletClient notification.
		
		phoneClient.addMessage("Here is a new message");
		tabletClient.addMessage("Another new message");

	}

}
