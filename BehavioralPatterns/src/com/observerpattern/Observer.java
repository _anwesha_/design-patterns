package com.observerpattern;

public abstract class Observer {
	
	protected Subject subject;
	abstract void update();

}
