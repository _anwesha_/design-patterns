package com.interpreterpattern;

public interface Expression {
	
	public boolean interpret(String context);

}
