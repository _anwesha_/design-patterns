package com.mementopattern;

//memento
public class EmployeeMemento {
	
	private String name;
	private String phone;//if we are implementing the serializable interface we can mark address as transient.
	
	public EmployeeMemento(String name, String phone) {
		this.name = name;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

}
