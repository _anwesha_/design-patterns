package com.visitorpattern;

public interface AtvPart {
	
	public void accept(AtvPartVisitor visitor);

}
