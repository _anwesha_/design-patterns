package com.templatepattern;

public class StoreOrder extends OrderTemplate {

	@Override
	public void doCheckout() {
		System.out.println("Ring items from cart.");
	}

	@Override
	public void doPayment() {
		System.out.println("Process payment with card.");
	}

	@Override
	public void doReceipt() {
		System.out.println("Bag items at counter.");
	}

	@Override
	public void doDelivery() {
		System.out.println("Print receipt.");
	}

}
