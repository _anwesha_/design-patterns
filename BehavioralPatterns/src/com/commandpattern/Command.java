package com.commandpattern;

//command
public interface Command {
	
	public void execute();

}
