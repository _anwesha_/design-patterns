package com.commandpattern;

//invoker
public class Switch {
	
	public void storeAndExecute(Command command) {
		command.execute();
	}

}
