package com.strategypattern;

public class VisaStrategy extends ValidationStrategy {

	@Override
	public boolean isValid(CreditCard creditCard) {

		boolean isValid = true;

		isValid = creditCard.getNumber().startsWith("4");

		if (isValid) {// reduces cyclomatic complexity.
			isValid = creditCard.getNumber().length() == 16;
		}

		if (isValid) {
			isValid = passesLuhn(creditCard.getNumber());
		}

		return isValid;

	}

}
