package com.strategypattern;

public class StrategyDemo {

	public static void main(String[] args) {

		CreditCard amexCard = new CreditCard(new AmexStrategy());

		amexCard.setNumber("379185883464283");
		amexCard.setDate("04/2020");
		amexCard.setCvv("123");

		System.out.println("Is Amex valid: " + amexCard.isValid());

		CreditCard anotherAmexCard = new CreditCard(new AmexStrategy());

		anotherAmexCard.setNumber("379185883464282");
		anotherAmexCard.setDate("04/2020");
		anotherAmexCard.setCvv("123");

		System.out.println("Is Amex valid: " + anotherAmexCard.isValid());
		
		CreditCard visaCard = new CreditCard(new VisaStrategy());

		visaCard.setNumber("4539589763663402");
		visaCard.setDate("04/2020");
		visaCard.setCvv("123");

		System.out.println("Is Visa valid: " + visaCard.isValid());

	}

}
