package com.mediatorpattern;

//receiver
public class Light {
	
	private boolean isOn=false;
	private String location="";
	
	public Light() {
		
	}
	
	public Light(String location) {
		this.location=location;
	}
	
	public boolean isOn() {
		return isOn;
	}
	
	public void toggle() {
		if(isOn) {
			System.out.print(location+" ");
			off();
			isOn=false;
		}
		else {
			System.out.print(location+" ");
			on();
			isOn=true;
		}
	}
	
	public void on() {
		System.out.println("Light switched on.");
	}
	
	public void off() {
		System.out.println("Light switched off.");
	}

}
