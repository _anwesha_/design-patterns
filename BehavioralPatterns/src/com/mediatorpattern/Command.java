package com.mediatorpattern;

//colleague
public interface Command {
	
	void execute();

}
